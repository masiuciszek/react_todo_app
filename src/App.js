import React from 'react';
import './App.css';
import uuid from 'uuid/v4';
import todosdb from './todosdb';
import TodoList from './components/TodoList';
import Navbar from './components/Navbar';
import TodoForm from './components/TodoForm';
import useAppState from './hooks/useAppState';

export default function App() {
  const initialTodos = todosdb;

  const { addTodo, deleteTodo, completeTodo, todos, editTodo } = useAppState(
    initialTodos
  );

  return (
    <>
      <Navbar title="Todos app with react hooks" />
      <div className="App">
        <TodoForm addTodo={addTodo} />
        <TodoList
          todos={todos}
          completeTodo={completeTodo}
          deleteTodo={deleteTodo}
          editTodo={editTodo}
        />
      </div>
    </>
  );
}
