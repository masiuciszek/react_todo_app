import React from 'react';
import styled from 'styled-components';
import useInput from '../hooks/useInput';

const EditForm = ({ editTodo, id, toggle, task }) => {
  const [value, handleChange, reset] = useInput('');

  const handleSubmit = e => {
    e.preventDefault();
    editTodo(id, value);
    reset();
    toggle();
  };

  return (
    <Form onSubmit={handleSubmit}>
      <input
        type="text"
        value={value}
        onChange={handleChange}
        placeholder={task}
      />
    </Form>
  );
};

const Form = styled.form`
  input {
    width: 30%;
    padding: 0.3rem 0.7rem;
    box-shadow: 3px 3px 2px 3px #111;
    border: none;
    border-radius: 1rem;
    font-size: 1.2rem;
    transition: 0.2s ease-in-out;
    &:hover {
      box-shadow: 5px 5px 5px 3px #111;
    }
  }
`;
export default EditForm;
