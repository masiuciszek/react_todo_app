import React from 'react';
import styled from 'styled-components';

const Navbar = ({ title }) => (
  <NavWrapper>
    <h4>{title}</h4>
  </NavWrapper>
);

const NavWrapper = styled.nav`
  background: #3a539b;
  padding: 2rem;
  color: #fefefe;
  h4 {
    font-size: 2rem;
    border-bottom: 2px solid #fefefe;
    width: 25%;
    letter-spacing: 0.3rem;
    line-height: 3rem;
  }
`;

export default Navbar;
