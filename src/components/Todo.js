/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
import React from 'react';
import styled from 'styled-components';
import { Edit, Delete } from 'styled-icons/material';
import EditForm from './EditForm';
import useToggle from '../hooks/useToggle';

const Todo = ({
  todo: { id, task, completed },
  completeTodo,
  deleteTodo,
  editTodo,
}) => {
  const [editing, toggle] = useToggle(false);

  return (
    <TodoWrapper>
      <div className="task-wrapper">
        <input
          type="checkbox"
          checked={completed}
          onClick={() => completeTodo(id)}
          id="checked"
        />
        <label htmlFor="checked" />

        <h4 className={completed ? 'check' : ''}>
          {editing ? (
            <EditForm id={id} toggle={toggle} task={task} editTodo={editTodo} />
          ) : (
            <h4>{task}</h4>
          )}
          <span>
            <Edit size="45" onClick={toggle} />
          </span>
          <span onClick={() => deleteTodo(id)}>
            <Delete size="45" />
          </span>{' '}
        </h4>
      </div>
    </TodoWrapper>
  );
};

const TodoWrapper = styled.div`
  display: flex;
  padding: 1rem;
  width: 100%;
  position: relative;
  border-bottom: 2px solid #ccc;
  .task-wrapper {
    display: flex;
    width: 100%;
    position: relative;
    input {
      margin: 1rem;
    }
    /* label {
      position: absolute;
      padding: 1rem;
      background: #ccc;
      border-radius: 16%;
      border: #3a539b 1px solid;
      &::after {
        content: '\f00c';
        font-family: 'FontAwesome';
        left: -31px;
        top: 2px;
        color: transparent;
        transition: color 0.2s;
      }
      &::before {
        content: '';
      }
    } */

    h4 {
      margin: 0.3rem;
      font-size: 2rem;
      width: 100%;
    }
    span {
      cursor: pointer;
      float: right;
    }
  }
  .check {
    text-decoration: line-through;
  }
`;

export default Todo;
