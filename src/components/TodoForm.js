import React from 'react';
import styled from 'styled-components';
import useInput from '../hooks/useInput';

const TodoForm = ({ addTodo }) => {
  const [value, handleChange, reset] = useInput('');
  const handleSubmit = e => {
    e.preventDefault();
    addTodo(value);
    reset();
  };
  return (
    <Form onSubmit={handleSubmit}>
      <input
        type="text"
        value={value}
        onChange={handleChange}
        placeholder="Add an todo..."
      />
    </Form>
  );
};

const Form = styled.form`
  display: flex;
  padding: 1rem;
  width: 60%;
  justify-content: center;
  margin: 0 auto;
  margin-top: 3rem;
  input {
    width: 70%;
    padding: 0.8rem 1.2rem;
    box-shadow: 3px 3px 2px 3px #111;
    border: none;
    border-radius: 1rem;
    font-size: 1.2rem;
    transition: 0.2s ease-in-out;
    &:hover {
      box-shadow: 5px 5px 5px 3px #111;
    }
  }
`;
export default TodoForm;
