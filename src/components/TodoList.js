import React from 'react';
import styled from 'styled-components';
import Todo from './Todo';

const TodoList = ({ todos, completeTodo, deleteTodo, editTodo }) => {
  if (todos.length) {
    return (
      <>
        <TodoListWrapper>
          {todos.map(todo => (
            <Todo
              key={todo.id}
              todo={todo}
              completeTodo={completeTodo}
              deleteTodo={deleteTodo}
              editTodo={editTodo}
            />
          ))}
        </TodoListWrapper>
      </>
    );
  }
  return null;
};

const TodoListWrapper = styled.div`
  padding: 1rem;
  box-shadow: 3px 3px 2px 3px #111;
  width: 55%;
  margin: 1rem auto;
  /* min-height: 30vh; */
  height: 100%;
  border: 3px solid #3a539b;
`;

export default TodoList;
