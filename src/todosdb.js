import uuid from 'uuid/v4';

export default [
  {
    id: uuid(),
    task: 'Go out with the dog ',
    completed: false,
  },
  {
    id: uuid(),
    task: 'Get some Food ',
    completed: false,
  },
  {
    id: uuid(),
    task: 'Shop some food',
    completed: false,
  },
];
